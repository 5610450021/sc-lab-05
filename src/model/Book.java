package model;
public class Book {
	private String bookName;
	private String years;
	private String status;
	
	public Book(String bookName, String years) {
		this.bookName = bookName;
		this.years = years;
		this.status = "";
	}

	public String getYears() {
		return years;
	}

	public void setYears(String years) {
		this.years = years;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
}
