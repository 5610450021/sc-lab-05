package model;

import java.util.ArrayList;

public class Library {
	private ArrayList<Book> bookAll = new ArrayList<Book>();
	private ArrayList<ReferencesBook> refAll = new ArrayList<ReferencesBook>();
	private boolean borbook;
	
	public void addBook(Book book) {
		bookAll.add(book);
	}
	
	public void removeBook(Book book){
		bookAll.remove(book);
	}

	public void addRefBook(ReferencesBook refBook) {
		refAll.add(refBook);
	}
	
	public void removeRefBook(ReferencesBook refBook) {
		refAll.remove(refBook);
	}
	
	public ArrayList<Book> getBook() {
		return bookAll;
	}
	
	public ArrayList<ReferencesBook> getBookRef(){
		return refAll;
	}
	
	public int getBookCount(){
		int sum = bookAll.size() + refAll.size();
		return sum;
	}

	public boolean borrowBook(Student stu, Book bookName) {
		
		if (bookName.getStatus() == "" && bookAll.contains(bookName)){
			bookName.setStatus("Unvailable");
			removeBook(bookName);
			borbook = true;
		}
		else {
			borbook = false;
		}
		
		return borbook;
	}
	
	public boolean borrowRef(Student stu, ReferencesBook bookRef) {
		return false;
	}
	
	public void returnBook(Student stu, Book bookName) {
		bookName.setStatus("");
		addBook(bookName);
		borbook = true;
		
	}
	
	public boolean returnRef(Student stu, ReferencesBook bookName) {
		return false;
		
	}

}
