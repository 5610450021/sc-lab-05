package model;
public class Student {
	private String name;
	private String id;
	private String subjectCode;
	
	public Student(String name, String id, String subjectCode) {
		this.name = name;
		this.id = id;
		this.subjectCode = subjectCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubjectCode() {
		return subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	

}
