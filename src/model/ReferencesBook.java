package model;
public class ReferencesBook {
	private String bookName;
	private String years;
	private String status;
	
	public ReferencesBook(String bookName, String years) {
		this.bookName = bookName;
		this.years = years;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getYears() {
		return years;
	}

	public void setYears(String years) {
		this.years = years;
	}
	
	
}
