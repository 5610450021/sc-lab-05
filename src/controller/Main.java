package controller;
import java.util.ArrayList;
import java.util.HashMap;

import model.Book;
import model.Library;
import model.ReferencesBook;
import model.Student;

public class Main {
	public Main(){
		Library lib = new Library();
		Student stu = new Student("Lalita", "5610450322", "D14");
		Student stu2 = new Student("Thamonwan", "5610450021", "D14");
		Book book1 = new Book("Big Java", "2012"); 
		Book book2 = new Book("PPL", "2014");
		Book book3 = new Book("Math", "2010");
		Book book4 = new Book("Linear", "2012");
		Book book5 = new Book("Algorithm", "2005");
		ReferencesBook refBook = new ReferencesBook("NewsPaper", "2015");
		ReferencesBook refBook2 = new ReferencesBook("Dictionary", "2009");
		
		lib.addBook(book1);
		lib.addBook(book2);
		lib.addBook(book3);
		lib.addBook(book4);
		lib.addBook(book5);
		lib.addRefBook(refBook);
		lib.addRefBook(refBook2);
		System.out.println(lib.getBookCount());
		getAllBook(lib.getBook());
		getAllBookRef(lib.getBookRef());
		
		System.out.println(lib.borrowBook(stu, book1));
		System.out.println(lib.borrowBook(stu, book5));
		System.out.println(lib.borrowBook(stu, book3));
		System.out.println(lib.getBookCount());				// 4
		lib.returnBook(stu, book1);
		lib.returnBook(stu, book3);
		System.out.println(lib.getBookCount());				// 6
		
		System.out.println(lib.borrowRef(stu2, refBook));	// false
		System.out.println(lib.borrowBook(stu2, book1));	
		System.out.println(lib.borrowBook(stu, book5));		// ���˹ѧ��ͷ���դ���������
		
		
//		HashMap hm = new HashMap();
//		hm.put("5610450322", new ArrayList<String>());
//		((ArrayList<String>) hm.get("5610450322")).add("Big Java");
//		System.out.println(hm.get("5610450322"));
	}

	public static void main(String[] args) {
		new Main();
	}
	
	public void getAllBook(ArrayList<Book> book){
		for (Book book2 : book) {
			System.out.println(book2.getBookName());
		}
	}	
	
	public void getAllBookRef(ArrayList<ReferencesBook> bookRef){
		for (ReferencesBook referencesBook : bookRef) {
			System.out.println(referencesBook.getBookName());
		}
	}	
}
